package com.sample.order.model.dto;

public class ShippingInfoDTO {
    private Long id;
    private Long orderId;
    private boolean leftOriginCountry;
    private boolean reachedDestinationCountry;
    private boolean sameDayShipping;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public boolean isLeftOriginCountry() {
        return leftOriginCountry;
    }

    public void setLeftOriginCountry(boolean leftOriginCountry) {
        this.leftOriginCountry = leftOriginCountry;
    }

    public boolean isReachedDestinationCountry() {
        return reachedDestinationCountry;
    }

    public void setReachedDestinationCountry(boolean reachedDestinationCountry) {
        this.reachedDestinationCountry = reachedDestinationCountry;
    }

    public boolean isSameDayShipping() {
        return sameDayShipping;
    }

    public void setSameDayShipping(boolean sameDayShipping) {
        this.sameDayShipping = sameDayShipping;
    }
}
