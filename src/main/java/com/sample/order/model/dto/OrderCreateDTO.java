package com.sample.order.model.dto;

public class OrderCreateDTO {
    private String itemName;
    private Long customerId;
    private boolean sameDayShipping;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public boolean isSameDayShipping() {
        return sameDayShipping;
    }

    public void setSameDayShipping(boolean sameDayShipping) {
        this.sameDayShipping = sameDayShipping;
    }
}
