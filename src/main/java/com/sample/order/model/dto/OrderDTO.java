package com.sample.order.model.dto;

import com.sample.order.model.OrderStatus;

public class OrderDTO {
    private String itemName;
    private OrderStatus orderStatus;
    private CustomerDTO customerDTO;
    private ShippingInfoDTO shippingInfoDTO;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public CustomerDTO getCustomerDTO() {
        return customerDTO;
    }

    public void setCustomerDTO(CustomerDTO customerDTO) {
        this.customerDTO = customerDTO;
    }

    public ShippingInfoDTO getShippingInfoDTO() {
        return shippingInfoDTO;
    }

    public void setShippingInfoDTO(ShippingInfoDTO shippingInfoDTO) {
        this.shippingInfoDTO = shippingInfoDTO;
    }
}
