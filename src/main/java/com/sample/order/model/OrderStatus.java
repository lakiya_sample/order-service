package com.sample.order.model;

public enum  OrderStatus {
    Pending, Accepted, Delivered;
}
