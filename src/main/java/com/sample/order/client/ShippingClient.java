package com.sample.order.client;

import com.sample.order.model.dto.ShippingInfoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("shipping")
public interface ShippingClient {
    @RequestMapping(method = RequestMethod.POST, value = "/shipping")
    ShippingInfoDTO saveShippingInfo(@RequestBody ShippingInfoDTO shippingInfoDTO);

    @RequestMapping(method = RequestMethod.PUT, value = "/shipping")
    ShippingInfoDTO updateShippingInfo(@RequestBody ShippingInfoDTO shippingInfoDTO);

    @RequestMapping(method = RequestMethod.GET, value = "/shipping/{orderId}")
    ShippingInfoDTO getShippingInfoByOrderId(@PathVariable("orderId") Long orderId);
}
