package com.sample.order.client;

import com.sample.order.model.dto.CustomerDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("customer")
public interface CustomerClient {
    @RequestMapping(method = RequestMethod.GET, value = "/customer/{customerId}")
    CustomerDTO getCustomerById(@PathVariable("customerId") Long customerId);
}
