package com.sample.order.controller;

import com.sample.order.model.Order;
import com.sample.order.model.OrderStatus;
import com.sample.order.model.dto.OrderCreateDTO;
import com.sample.order.model.dto.OrderDTO;
import com.sample.order.model.dto.OrderUpdateDTO;
import com.sample.order.repository.OrderRepository;
import com.sample.order.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("order")
public class OrderController {
    private final OrderRepository orderRepository;
    private final OrderService orderService;

    public OrderController(OrderRepository orderRepository,
                           OrderService orderService) {
        this.orderRepository = orderRepository;
        this.orderService = orderService;
    }

    @PostMapping
    public ResponseEntity<Order> createOrder(@RequestBody OrderCreateDTO orderCreateDTO){
        Order order = this.orderService.createOrder(orderCreateDTO);
        return new ResponseEntity<>(order, HttpStatus.CREATED);
    }

    @GetMapping("{orderId}")
    public ResponseEntity<OrderDTO> findOrderById(@PathVariable Long orderId){
        OrderDTO orderDTO =  this.orderService.findOrder(orderId);
        return new ResponseEntity<>(orderDTO, HttpStatus.OK);
    }

    @GetMapping("searchByOrderStatus")
    public List<OrderDTO> findOrdersByStatus(@RequestParam OrderStatus orderStatus){
        return this.orderService.findByOrderStatus(orderStatus);
    }

    @PutMapping
    public ResponseEntity<Order> updateOrder(@RequestBody OrderUpdateDTO orderUpdateDTO){
        Order order = this.orderService.updateOrder(orderUpdateDTO);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @PutMapping("updateOrderStatus/{orderId}")
    public ResponseEntity<Order> updateOrderStatus(@PathVariable Long orderId, @RequestParam OrderStatus orderStatus){
        Order order = this.orderService.updateOrderStatus(orderId, orderStatus);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }
}