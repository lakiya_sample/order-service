package com.sample.order.service;

import com.sample.order.client.CustomerClient;
import com.sample.order.client.ShippingClient;
import com.sample.order.model.Order;
import com.sample.order.model.OrderStatus;
import com.sample.order.model.dto.*;
import com.sample.order.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final CustomerClient customerClient;
    private final ShippingClient shippingClient;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, CustomerClient customerClient, ShippingClient shippingClient) {
        this.orderRepository = orderRepository;
        this.customerClient = customerClient;
        this.shippingClient = shippingClient;
    }

    @Override
    public Order createOrder(OrderCreateDTO orderCreateDTO) {
        Order order = new Order();
        CustomerDTO customerDTO = this.customerClient.getCustomerById(orderCreateDTO.getCustomerId());
        if (customerDTO != null) {
            order.setCustomerId(orderCreateDTO.getCustomerId());
            order.setItemName(orderCreateDTO.getItemName());
            order.setOrderStatus(OrderStatus.Pending);
            order.setOrderStatus(OrderStatus.Pending);
            order = this.orderRepository.saveAndFlush(order);

            ShippingInfoDTO shippingInfo = new ShippingInfoDTO();
            shippingInfo.setLeftOriginCountry(false);
            shippingInfo.setReachedDestinationCountry(false);
            shippingInfo.setSameDayShipping(orderCreateDTO.isSameDayShipping());
            shippingInfo.setOrderId(order.getId());

            shippingInfo = this.shippingClient.saveShippingInfo(shippingInfo);
            order.setShippingInfoId(shippingInfo.getId());
            this.orderRepository.save(order);
            return order;
        } else {
            throw new RuntimeException("Customer not found with give id");
        }
    }

    @Override
    public Order updateOrder(OrderUpdateDTO orderUpdateDTO) {
        Optional<Order> optOrder = this.orderRepository.findById(orderUpdateDTO.getOrderId());
        if (optOrder.isPresent()) {
            Order order = optOrder.get();
            order.setOrderStatus(orderUpdateDTO.getOrderStatus());
            order.setItemName(orderUpdateDTO.getItemName());
            ShippingInfoDTO shippingInfo = new ShippingInfoDTO();
            shippingInfo.setOrderId(order.getId());
            shippingInfo.setSameDayShipping(orderUpdateDTO.isSameDayShipping());
            shippingInfo.setReachedDestinationCountry(orderUpdateDTO.isReachedDestinationCountry());
            shippingInfo.setLeftOriginCountry(orderUpdateDTO.isLeftOriginCountry());
            this.shippingClient.updateShippingInfo(shippingInfo);
            return this.orderRepository.save(order);
        } else {
            throw new RuntimeException("Order not found with give id");
        }
    }

    @Override
    public Order updateOrderStatus(Long orderId, OrderStatus orderStatus) {
        Optional<Order> optOrder = this.orderRepository.findById(orderId);
        if (optOrder.isPresent()) {
            Order order = optOrder.get();
            order.setOrderStatus(orderStatus);
            return this.orderRepository.save(order);
        } else {
            throw new RuntimeException("Order not found with give id");
        }
    }

    @Override
    public OrderDTO findOrder(Long orderId) {
        Optional<Order> optOrder = this.orderRepository.findById(orderId);
        if (optOrder.isPresent()) {
            Order order = optOrder.get();
            ShippingInfoDTO shippingInfoDTO = this.shippingClient.getShippingInfoByOrderId(orderId);
            CustomerDTO customerDTO = this.customerClient.getCustomerById(order.getCustomerId());
            OrderDTO orderDTO = new OrderDTO();
            orderDTO.setItemName(order.getItemName());
            orderDTO.setOrderStatus(order.getOrderStatus());
            orderDTO.setShippingInfoDTO(shippingInfoDTO);
            orderDTO.setCustomerDTO(customerDTO);
            return orderDTO;
        } else {
            throw new RuntimeException("Order not found with give id");
        }
    }

    @Override
    public List<OrderDTO> findByOrderStatus(OrderStatus orderStatus) {
        List<Order> orderList = this.orderRepository.findByOrderStatus(orderStatus);
        List<OrderDTO> orderDTOList = new ArrayList<>();
        for (Order order : orderList) {
            ShippingInfoDTO shippingInfoDTO = this.shippingClient.getShippingInfoByOrderId(order.getId());
            CustomerDTO customerDTO = this.customerClient.getCustomerById(order.getCustomerId());
            OrderDTO orderDTO = new OrderDTO();
            orderDTO.setItemName(order.getItemName());
            orderDTO.setOrderStatus(order.getOrderStatus());
            orderDTO.setShippingInfoDTO(shippingInfoDTO);
            orderDTO.setCustomerDTO(customerDTO);
            orderDTOList.add(orderDTO);
        }
        return orderDTOList;
    }
}
