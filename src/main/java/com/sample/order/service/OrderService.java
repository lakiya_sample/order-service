package com.sample.order.service;

import com.sample.order.model.Order;
import com.sample.order.model.OrderStatus;
import com.sample.order.model.dto.OrderCreateDTO;
import com.sample.order.model.dto.OrderDTO;
import com.sample.order.model.dto.OrderUpdateDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface OrderService {
    Order createOrder(OrderCreateDTO orderCreateDTO);

    Order updateOrder(OrderUpdateDTO orderUpdateDTO);

    Order updateOrderStatus(Long orderId, OrderStatus orderStatus);

    OrderDTO findOrder(Long orderId);

    List<OrderDTO> findByOrderStatus(OrderStatus orderStatus);
}
