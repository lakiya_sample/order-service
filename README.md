# Order Service

Order Microservice of order processing system

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Docker 

Linux Based environment or Windows environment that can run .sh

```
You can run .sh with git bash for windows
```

### Installing

Run the build.sh in project directory

```
./build.sh
```

Open run.sh and change DOCKER_HOST ip to IP of your computer.

```
docker run -p 8083:8083 -e SPRING_PROFILES_ACTIVE=external -e DOCKER_HOST=your_ip_here order-service:latest
```

Run the run.sh in project directory

```
./run.sh
```

This will start your application on port 8081

## Access Swagger for API Documentation

[Swagger](http://localhost:8081/swagger-ui.html/) 